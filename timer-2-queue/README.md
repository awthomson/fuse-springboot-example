# Example Fuse SpringBoot app

To compile:
````
mvn compile`
````

To build docker image:
````
docker build -t springboot-demo .
````

To run:
````
docker run -d springboot-demo
````

To start a AMQ container to connect to:
````
docker run -d -p 8161:8161 -p 61616:61616 rmohr/activemq
````